#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
. $DIR/env.conf

OIAM_V41_BASE="/usr/local/OpenIAM"
OIAM_V42_BASE="/usr/local/openiam"

SOURCE_FILE=$1
if [[ -z "$SOURCE_FILE" ]]; then
  echo "Please specify backup file to restore the instance."
  echo "./restore.sh <backup_file_location_here>"
  echo "You also can specify file on remore server like"
  echo "./restore.sh user@server.com:/storage/file.backup"
  echo "The variables in env.conf must be set"
  exit 2
fi

TEMP_FOLDER=$(date '+%Y-%m-%d_%H-%M')
mkdir -p $DIR/$TEMP_FOLDER

BASE_NAME=$(basename $SOURCE_FILE)

if [[ $SOURCE_FILE == *"@"* ]]; then
  echo "Downloading from $SOURCE_FILE ..."
  scp -P $HOST_PORT -i $AUTH_CERT_PATH $SOURCE_FILE ${DIR}/${TEMP_FOLDER}/
elif [[ ! -f $SOURCE_FILE ]]; then
  echo "File $SOURCE_FILE doesn't exist"
  exit 2
else
  echo "Local file $SOURCE_FILE has been found."
  cp $SOURCE_FILE ${DIR}/${TEMP_FOLDER}/
fi

echo "EXTRACTING BACKUP"
tar -xzf ${DIR}/${TEMP_FOLDER}/$BASE_NAME --directory ${DIR}/${TEMP_FOLDER}
rm -rf ${DIR}/${TEMP_FOLDER}/$BASE_NAME

REGEXP="(.*?)__(.*?).backup"
BACKUP_DATE=""
BACKUP_VERSION=""
if [[ $BASE_NAME =~ $REGEXP ]]
then
    BACKUP_DATE="${BASH_REMATCH[1]}"
    BACKUP_VERSION="${BASH_REMATCH[2]}"
else
    echo "backup is corrupted."
    exit 3
fi

echo "PLEASE CONFIRM THAT YOU WANT TO RESTORE TO OPENIAM $BACKUP_VERSION VERSION FROM $BACKUP_DATE "
echo "Only 'yes' answer is possible to continue"
read KEYBOARD_INPUT
if [[ $KEYBOARD_INPUT != "yes" ]]; then
  echo "GOOD BYE!"
  exit 4
fi

echo "STOPPING OPENIAM"
systemctl stop openiam-auth
systemctl stop openiam-device
systemctl stop openiam-email
systemctl stop openiam-esb
systemctl stop openiam-groovy
systemctl stop openiam-idm
systemctl stop openiam-reconciliation
systemctl stop openiam-synchronization
systemctl stop openiam-ui
systemctl stop openiam-workflow
systemctl stop elasticsearch

if [[ $BACKUP_VERSION == 4.2.* ]]; then
  systemctl stop openiam-vault
  systemctl stop etcd
fi
echo "Wait 60s while OpenIAM stops"
sleep 60

SOURCE_OF_DATA="${DIR}/${TEMP_FOLDER}/${BACKUP_DATE}/${BACKUP_VERSION}"

if [[ $BACKUP_VERSION == 4.2.* ]]; then

  rm $OIAM_V42_BASE/connectors/bin/*.jar
  cp -rf $SOURCE_OF_DATA/connectors/*.jar $OIAM_V42_BASE/connectors/bin/
  chown -R openiam:openiam $SOURCE_OF_DATA/connectors/

  rm -rf $OIAM_V42_BASE/ui/webapps/*
  cp -rf $SOURCE_OF_DATA/frontend/* $OIAM_V42_BASE/ui/webapps/
  chown -R openiam:openiam $OIAM_V42_BASE/ui/webapps/

  rm -rf $OIAM_V42_BASE/services/bin/*
  cp -rf $SOURCE_OF_DATA/binaries/* $OIAM_V42_BASE/services/bin/
  chown -R openiam:openiam $OIAM_V42_BASE/services/bin/

  rm -rf $OIAM_V42_BASE/conf/*
  cp -rf $SOURCE_OF_DATA/conf/* $OIAM_V42_BASE/conf/
  chown -R openiam:openiam $OIAM_V42_BASE/conf/

  rm -rf /var/lib/elasticsearch/*
  cp -rf $SOURCE_OF_DATA/elasticsearch/* /var/lib/elasticsearch/
  chown -R elasticsearch:elasticsearch /var/lib/elasticsearch/

  rm -rf /var/lib/etcd/*
  cp -rf $SOURCE_OF_DATA/etcd/* /var/lib/etcd/
  chown -R etcd:etcd /var/lib/etcd/

  rm -rf $OIAM_V42_BASE/vault/certs/*
  cp -rf $SOURCE_OF_DATA/vault/* $OIAM_V42_BASE/vault/certs/
  chown -R vault:vault $OIAM_V42_BASE/vault/certs/
elif [[ $BACKUP_VERSION == 4.1.* ]]; then
  rm -rf $OIAM_V41_BASE/connectors/*
  cp -rf $SOURCE_OF_DATA/connectors/*.jar $OIAM_V41_BASE/connectors/
  chown -R openiam:openiam $SOURCE_OF_DATA/connectors/

  rm -rf $OIAM_V41_BASE/tomcat/webapps/*
  cp -rf $SOURCE_OF_DATA/frontend/* $OIAM_V41_BASE/tomcat/webapps/
  chown -R openiam:openiam $OIAM_V41_BASE/tomcat/webapps/

  rm -rf $OIAM_V41_BASE/*.jar
  cp -rf $SOURCE_OF_DATA/binaries/* $OIAM_V41_BASE/
  chown -R openiam:openiam $OIAM_V41_BASE/*

  rm -rf $OIAM_V41_BASE/data/openiam/conf/*
  cp -rf $SOURCE_OF_DATA/conf/* $OIAM_V41_BASE/data/openiam/conf/
  chown -R openiam:openiam $OIAM_V41_BASE/data/openiam/conf/

  rm -rf /var/lib/elasticsearch/*
  cp -rf $SOURCE_OF_DATA/elasticsearch/* /var/lib/elasticsearch/
  chown -R elasticsearch:elasticsearch /var/lib/elasticsearch/
fi


systemctl start elasticsearch

if [[ $BACKUP_VERSION == 4.2.* ]]; then
systemctl start etcd
sleep 10
systemctl start openiam-vault
sleep 10
/usr/local/openiam/utils/vault/start.sh
fi

systemctl start openiam-auth
systemctl start openiam-device
systemctl start openiam-email
systemctl start openiam-esb
systemctl start openiam-groovy
systemctl start openiam-idm
systemctl start openiam-reconciliation
systemctl start openiam-synchronization
systemctl start openiam-ui
systemctl start openiam-workflow

echo "OpenIAM is restored to version $BACKUP_VERSION from date $BACKUP_DATE"
