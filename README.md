# README #

This README describes how to use OpenIAM backup / repair utility.
This utility works only with 1 tier RPM installed OpenIAM isntances versions 4.1.X and 4.2.X

Based on your environment confuguration fill free to modify scripts by your needs.

OpenIAM team strongly recommend you to make backup before any OpenIAM maintain operations.
The following files must be backed up:
- conf folder;
- jar and war files of the backend and frontend;
- vault secrets and certificates ( > 4.2);
- elasticsearch storage files;
- database dump (out of this utility). Please read more information about database backups below.

To make a backup file run backup.sh script from this folder

```
  ./backup.sh
```

The minimal backup size is 2GB. But it might be significantly bigger due to the number of users and object in your OpenIAM instance.

You can upload or download the backup file to/from remote server. To reach this please fill the variables in file env.conf with values

| NAME | DESCRIPTION | DEFAULT VALUE |
|-----:|:------------|---------------|
| HOST_NAME | The host name of remote server there backup file will be uploaded | _empty_ |
| HOST_PORT | The port of remote server there backup file will be uploaded  | 22 |
| USER_NAME | Username that will be used to upload file  | _empty_ |
| AUTH_CERT_PATH | The certificate that will be used to authenticate on remote server | ~/.ssh/id_rsa |
| REMOTE_PATH | The folder on the remote server where backup will be placed  | ~/ |

To restore OpenIAM from previously created backup perform command

```
   ./restore.sh <name_of_backup_file>
```

The utility will stop OpenIAM rollback all stored files and start OpenIAM instance again from the time when backup has been done.

You also can point to the remote server storage to apply backup from there. Simply run the following command:
```
   ./restore.sh user@openiam.com:/opt/openiam/backups/2020-01-01_00-00__4.2.0.0.backup
```
Please don't forget to fill AUTH_CERT_PATH and HOST_PORT in env.conf folder before.
> This is IMPORTANT TO use username and server name format as the utility argument
> The path *openiam.com:/opt/openiam/backups/2020-01-01_00-00__4.2.0.0.backup* will not work.
