#!/bin/bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $DIR/env.conf
cd $DIR

folder=$(date '+%Y-%m-%d_%H-%M')
mkdir -p "./$folder"
OIAM_V41_BASE="/usr/local/OpenIAM"
OIAM_V42_BASE="/usr/local/openiam"
OIAM_V41="$OIAM_V41_BASE/tomcat/webapps/openiam-ui-static/META-INF/MANIFEST.MF"
OIAM_V42="$OIAM_V42_BASE/ui/webapps/openiam-ui-static/META-INF/MANIFEST.MF"
CUR_VERSION=""
if [[ -f $OIAM_V41 ]]; then
  CUR_VERSION="$(cat $OIAM_V41 | grep Openiam-Version | cut -d ' ' -f2 | sed 's/.$//')"
fi

if [[ -f $OIAM_V42 ]]; then
  CUR_VERSION="$(cat $OIAM_V42 | grep Openiam-Version | cut -d ' ' -f2 | sed 's/.$//')"
fi

echo "DETECTED $CUR_VERSION VERSION"

mkdir -p "$folder/$CUR_VERSION/conf" \
         "$folder/$CUR_VERSION/binaries" \
         "$folder/$CUR_VERSION/frontend" \
         "$folder/$CUR_VERSION/connectors" \
         "$folder/$CUR_VERSION/elasticsearch"
#         "$folder/$CUR_VERSION/vault" \

if [[ $CUR_VERSION == 4.2.* ]]; then
   echo "COPYING FOR $CUR_VERSION"
   cp -rf $OIAM_V42_BASE/connectors/bin/*.jar $folder/$CUR_VERSION/connectors/
   cp -rf $OIAM_V42_BASE/ui/webapps/*.war $folder/$CUR_VERSION/frontend/
   cp -rf $OIAM_V42_BASE/services/bin/* $folder/$CUR_VERSION/binaries/
   cp -rf $OIAM_V42_BASE/conf/* $folder/$CUR_VERSION/conf/
   cp -rf /var/lib/elasticsearch/*  $folder/$CUR_VERSION/elasticsearch/
   mkdir -p "$folder/$CUR_VERSION/vault"
   mkdir -p "$folder/$CUR_VERSION/etcd"
   cp -rf $OIAM_V42_BASE/vault/certs/* $folder/$CUR_VERSION/vault/
   cp -rf /var/lib/etcd/* $folder/$CUR_VERSION/etcd/
elif [[ $CUR_VERSION == 4.1.* ]]; then
  echo "COPYING FOR $CUR_VERSION"
  cp -rf $OIAM_V41_BASE/*.jar $folder/$CUR_VERSION/binaries/
  cp -rf $OIAM_V41_BASE/tomcat/webapps/*.war $folder/$CUR_VERSION/frontend/
  cp -rf $OIAM_V41_BASE/connectors/* $folder/$CUR_VERSION/connectors/
  cp -rf $OIAM_V41_BASE/data/openiam/conf/* $folder/$CUR_VERSION/conf/
  cp -rf /var/lib/elasticsearch/*  $folder/$CUR_VERSION/elasticsearch/

else
  echo "VERSION $CUR_VERSION CAN'T BE RECOGNIZED. EXITING WITH 2."
  exit 2
fi

FILENAME="${folder}__${CUR_VERSION}.backup"

echo "CREATING BACKUP"
tar -czf $FILENAME "$folder/$CUR_VERSION"
rm -rf "$folder"


if [[ -z $HOST_NAME || -z $USER_NAME || -z $AUTH_CERT_PATH || -z $REMOTE_PATH || -z $HOST_PORT ]]; then
  echo "Remote backup upload is not configured. backup file $FILENAME will be stored in $DIR/ on this machine"
  echo "To Enable Upload to remote storage please set up values for all variables in $DIR/env.conf file"
else
  echo "Copying $DIR/$FILENAME to remote server ${USER_NAME}@${HOST_NAME}:${REMOTE_PATH}"
  scp -P $HOST_PORT -i $AUTH_CERT_PATH $DIR/$FILENAME ${USER_NAME}@${HOST_NAME}:${REMOTE_PATH}
fi
